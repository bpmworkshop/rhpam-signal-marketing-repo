package ro.bull.bpm.examples.loan;

/**
 * This class was automatically generated by the data modeler tool.
 */
@org.kie.api.definition.type.Label("Loan Application")
public class LoanApplication implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Position(3)
	@org.kie.api.definition.type.Label("Income")
	private java.lang.Float income;

	@org.kie.api.definition.type.Position(0)
	@org.kie.api.definition.type.Label("Age")
	private java.lang.Integer age;

	@org.kie.api.definition.type.Position(2)
	@org.kie.api.definition.type.Label("Loan period")
	private java.lang.Integer period;

	@org.kie.api.definition.type.Label("Name")
	private java.lang.String name;

	@org.kie.api.definition.type.Label("Loan amount")
	private java.lang.Float amount;

	public LoanApplication() {
	}

	public java.lang.Float getIncome() {
		return this.income;
	}

	public void setIncome(java.lang.Float income) {
		this.income = income;
	}

	public java.lang.Integer getAge() {
		return this.age;
	}

	public void setAge(java.lang.Integer age) {
		this.age = age;
	}

	public java.lang.Integer getPeriod() {
		return this.period;
	}

	public void setPeriod(java.lang.Integer period) {
		this.period = period;
	}

	public java.lang.String getName() {
		return this.name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.Float getAmount() {
		return this.amount;
	}

	public void setAmount(java.lang.Float amount) {
		this.amount = amount;
	}

	public LoanApplication(java.lang.Float income, java.lang.Integer age,
			java.lang.Integer period, java.lang.String name,
			java.lang.Float amount) {
		this.income = income;
		this.age = age;
		this.period = period;
		this.name = name;
		this.amount = amount;
	}

	public LoanApplication(java.lang.Integer age, java.lang.Integer period,
			java.lang.Float income) {
		this.age = age;
		this.period = period;
		this.income = income;
	}

}
